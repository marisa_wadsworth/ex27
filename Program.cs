﻿using System;
using System.Collections.Generic;

namespace ex27
{
    class Items
    {
        public double Price;
        public int Quantity;

        public Items()
        {
            this.Price = 0;
            this.Quantity = 0;
        }
    }

    class Checkout
    {
        private List<Items> ItemList;

        public Checkout()
        {
            ItemList = new List<Items>();
        }

        public void Purchase(double price, int quantity)
        {
            var item = new Items();
            
            item.Price = price;
            item.Quantity = quantity;

            this.ItemList.Add(item);
        }

        public double getGST(double price, double rate = 15)
        {
            return price * rate/100;
        }

        public double getSubTotal()
        {
            double total = 0.00;
            foreach(var item in this.ItemList)
            {
                total = total + item.Price * item.Quantity;
            }
            return total;
        }

        public double getGrandTotal()
        {
            double subTotal = this.getSubTotal();
            double GST = this.getGST(subTotal);

            return subTotal + GST;
        }

        public double getGrandTotal(double subTotal, double GST)
        {
            return subTotal + GST;
        }

        public void PrintOutReceipt()
        {
            Console.WriteLine(".............................");
            Console.WriteLine("      Your purchase list");
            Console.WriteLine(".............................");

            var x = 0;
            foreach(var item in this.ItemList)
            {
                Console.WriteLine($" Product{x}      :{item.Price:C2} x {item.Quantity} = {item.Price * item.Quantity:C2}");
            }

            var subTotal = this.getSubTotal();
            var GST = this.getGST(subTotal);
            var gradnTotal = this.getGrandTotal();

            Console.WriteLine("------------------------------------");
            Console.WriteLine($" Sub Total   : {this.getSubTotal():C2}");
            Console.WriteLine($" GST         : {this.getGST(subTotal):C2}");
            Console.WriteLine($" Grand Total : {this.getGrandTotal(subTotal, GST):C2}");
            Console.WriteLine($"------------------------------------");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            var checkout = new Checkout();

            double price;
            int quantity;
            string answer;
            bool isAdded = false;

            Console.WriteLine(".............................");
            Console.WriteLine("   Welcome to Missa's Shop");
            Console.WriteLine(".............................");

            do
            {
                Console.Write("How much is your first item ?");
                price = double.Parse(Console.ReadLine());

                Console.Write("How many Items do you want to buy ?");
                quantity = int.Parse(Console.ReadLine());

                checkout.Purchase(price, quantity);

                Console.Write("Would you like to add another item ? ( yes or no)");
                answer = Console.ReadLine();
                isAdded = (answer.ToLower() == "yes" || answer.ToLower() == "y") ? true : false;

                Console.WriteLine("");

            } while(isAdded);

            checkout.PrintOutReceipt();
            Console.WriteLine("");

            Console.WriteLine(".............................");
            Console.WriteLine("        Thank You !");
            Console.WriteLine(".............................");
        }
    }
}
